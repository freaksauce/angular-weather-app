'use strict'

function WeatherCtrl ($scope, WeatherFactory) {
	
	function fetchWeather(zip) {
		WeatherFactory.getWeather(zip).then(function(data) {
			console.log(data);
			$scope.place = data;
		})
	}

	fetchWeather('84105');

	$scope.findWeather = function(zip) {
		$scope.place = '';
		fetchWeather(zip);
	};

}

function WeatherFactory($http, $q) {
  
	 function getWeather(zip) {
		var deferred = $q.defer();
		$http.get('https://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20weather.forecast%20WHERE%20location%3D%22' + zip + '%22&format=json&diagnostics=true&callback=')
		.success(function(data) {
			deferred.resolve(data.query.results.channel);
		})
		.error(function(err) {
			console.log('Error');
			deferred.reject(err);
		});
		return deferred.promise;
	}

	return {
		getWeather: getWeather
	};

}

WeatherCtrl.$inject = ['$scope', 'WeatherFactory'];
WeatherFactory.$inject = ['$http', '$q'];

angular
  .module('app', [])
  .factory('WeatherFactory', WeatherFactory)
  .controller('WeatherCtrl', WeatherCtrl);